#!/usr/bin/env python2.7

"""
Columbia W4111 Intro to databases
Example webserver

To run locally

    python server.py

Go to http://localhost:8111 in your browser


A debugger such as "pdb" may be helpful for debugging.
Read about it online.
"""

import os
import re
import hashlib
import datetime
from sqlalchemy import *
from sqlalchemy.pool import NullPool
from flask import Flask, request, render_template, g, redirect, Response, session

tmpl_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'templates')
app = Flask(__name__, template_folder=tmpl_dir)

app.secret_key = 'coms4111 development key'
inventories = []

#
# The following uses the postgresql test.db -- you can use this for debugging purposes
# However for the project you will need to connect to your Part 2 database in order to use the
# data
#
# XXX: The URI should be in the format of: 
#
#     postgresql://USER:PASSWORD@<IP_OF_POSTGRE_SQL_SERVER>/postgres
#
# For example, if you had username ewu2493, password foobar, then the following line would be:
#
#     DATABASEURI = "postgresql://ewu2493:foobar@<IP_OF_POSTGRE_SQL_SERVER>/postgres"
#
# Swap out the URI below with the URI for the database created in part 2
# DATABASEURI = "sqlite:///test.db"
DATABASEURI = "postgresql://sk3961:gq6se@104.196.175.120/postgres"


#
# This line creates a database engine that knows how to connect to the URI above
#
engine = create_engine(DATABASEURI)


#
# START SQLITE SETUP CODE
#
# after these statements run, you should see a file test.db in your webserver/ directory
# this is a sqlite database that you can query like psql typing in the shell command line:
# 
#     sqlite3 test.db
#
# The following sqlite3 commands may be useful:
# 
#     .tables               -- will list the tables in the database
#     .schema <tablename>   -- print CREATE TABLE statement for table
# 
# The setup code should be deleted once you switch to using the Part 2 postgresql database
#
# engine.execute("""DROP TABLE IF EXISTS test;""")
# engine.execute("""CREATE TABLE IF NOT EXISTS test (
#   id serial,
#   name text
# );""")
# engine.execute("""INSERT INTO test(name) VALUES ('grace hopper'), ('alan turing'), ('ada lovelace');""")
#
# END SQLITE SETUP CODE
#



@app.before_request
def before_request():
  """
  This function is run at the beginning of every web request 
  (every time you enter an address in the web browser).
  We use it to setup a database connection that can be used throughout the request

  The variable g is globally accessible
  """
  try:
    g.conn = engine.connect()
  except:
    print "uh oh, problem connecting to database"
    import traceback; traceback.print_exc()
    g.conn = None

@app.teardown_request
def teardown_request(exception):
  """
  At the end of the web request, this makes sure to close the database connection.
  If you don't the database could run out of memory!
  """
  try:
    g.conn.close()
  except Exception as e:
    pass


#
# @app.route is a decorator around index() that means:
#   run index() whenever the user tries to access the "/" path using a GET request
#
# If you wanted the user to go to e.g., localhost:8111/foobar/ with POST or GET then you could use
#
#       @app.route("/foobar/", methods=["POST", "GET"])
#
# PROTIP: (the trailing / in the path is important)
# 
# see for routing: http://flask.pocoo.org/docs/0.10/quickstart/#routing
# see for decorators: http://simeonfranklin.com/blog/2012/jul/1/python-decorators-in-12-steps/
#
@app.route('/')
def index():
  """
  request is a special object that Flask provides to access web request information:

  request.method:   "GET" or "POST"
  request.form:     if the browser submitted a form, this contains the data in the form
  request.args:     dictionary of URL arguments e.g., {a:1, b:2} for http://localhost?a=1&b=2

  See its API: http://flask.pocoo.org/docs/0.10/api/#incoming-request-data
  """

  # DEBUG: this is debugging code to see what request looks like
  print request.args


  #
  # example of a database query
  #
  q = "SELECT * FROM {name}".format(name='users')
  cursor = g.conn.execute(q)
  # cursor = g.conn.execute("SELECT * FROM users")
  # cursor = g.conn.execute("SELECT * FROM customers")
  # cursor = g.conn.execute("SELECT * FROM administrators")
  # cursor = g.conn.execute("SELECT * FROM restaurants")
  # cursor = g.conn.execute("SELECT * FROM reviews")
  # cursor = g.conn.execute("SELECT * FROM inventories")
  # cursor = g.conn.execute("SELECT * FROM reservations")
  # cursor = g.conn.execute("SELECT * FROM coupons")
  # cursor = g.conn.execute("SELECT * FROM coupon_uses")
  # cursor = g.conn.execute("SELECT * FROM favorite_restaurants")
  users = []
  # emails = []
  for result in cursor:
    # names.append(result['name'])  # can also be accessed using result[0]
    # emails.append(result['email'])
    users.append(result)
  cursor.close()


  #
  # Flask uses Jinja templates, which is an extension to HTML where you can
  # pass data to a template and dynamically generate HTML based on the data
  # (you can think of it as simple PHP)
  # documentation: https://realpython.com/blog/python/primer-on-jinja-templating/
  #
  # You can see an example template in templates/index.html
  #
  # context are the variables that are passed to the template.
  # for example, "data" key in the context variable defined below will be 
  # accessible as a variable in index.html:
  #
  #     # will print: [u'grace hopper', u'alan turing', u'ada lovelace']
  #     <div>{{data}}</div>
  #     
  #     # creates a <div> tag for each element in data
  #     # will print: 
  #     #
  #     #   <div>grace hopper</div>
  #     #   <div>alan turing</div>
  #     #   <div>ada lovelace</div>
  #     #
  #     {% for n in data %}
  #     <div>{{n}}</div>
  #     {% endfor %}
  #
  context = dict(data = users)


  #
  # render_template looks in the templates/ folder for files.
  # for example, the below file reads template/index.html
  #
  return render_template("index.html", **context)

#
# This is an example of a different path.  You can see it at
# 
#     localhost:8111/another
#
# notice that the function name is another() rather than index()
# the functions for each app.route needs to have different names
#
@app.route('/register')
def register():
  return render_template("register.html")


@app.route('/administrator')
def administrator():
  # TODO : implement ADDing coupons feature
  return render_template("administrator.html")


@app.route('/customer')
def customer():
  # TODO : implement My Page
  # TODO :    show my reservation / my reviews / my favorite restaurants
  # TODO : implement SEARCH feature
  # TODO :    name / category / avg.rating
  # TODO :    if condition doesn't exist, show 'no such data'
  # TODO :    show optional reviews feature
  # TODO :    show optional BOOK feature
  # TODO :      when clicked, show available time slots with 'reserve' button
  # TODO :      if 'reserve' clicked, ADD to reservations and DELETE from inventories
  # TODO : implement WRITE REVIEWS feature
  # TODO : implement FAVORITE feature

  query_adm = 'SELECT * FROM reservations WHERE uid = ' + str(session['uid'])
  cursor_adm = g.conn.execute(query_adm)
  reservations = []
  for reservation in cursor_adm:

    query_adm_1 = 'SELECT * FROM restaurants WHERE rid = ' + str(reservation['rid'])
    cursor_adm_1 = g.conn.execute(query_adm_1)
    restaurants = []
    for restaurant in cursor_adm_1:
      restaurants.append(restaurant['name'])
    cursor_adm_1.close()
    restaurant_name = restaurants[0]

    reservations.append((restaurant_name, reservation['reserv_time'], reservation['table_size']))
  
  cursor_adm.close()

  if session['no_result']:
    session['no_result'] = False
    return render_template("customer.html", reservations=reservations, no_result=True)
  else:
    return render_template("customer.html", reservations=reservations, no_result=False)


# @app.route('/login')
# def login():
#   abort(401)
#   this_is_never_executed()


# Login process
@app.route('/login', methods=['POST'])
def login():
  email = request.form['email']
  password = hashlib.md5(request.form['password']).hexdigest()

  # TODO : check formats for email use regular expressions

  query_adm = 'SELECT * FROM users WHERE email = ' + '\''+ email + '\''+ ' AND password = ' + '\''+ password + '\''
  cursor_adm = g.conn.execute(query_adm)
  users = []
  for user in cursor_adm:
    users.append((user['uid'], user['name'], user['email'],user['password']))
  cursor_adm.close()
  
  if not users:
    return render_template('index.html', failed=True)

  uid = users[0][0]
  session['uid'] = int(uid)

  query_adm = 'SELECT * FROM administrators WHERE user_uid = ' + str(uid)
  cursor_adm = g.conn.execute(query_adm)
  administrators = []
  for user in cursor_adm:
    administrators.append((user['user_uid']))
  cursor_adm.close()

  if administrators:
    return redirect('/administrator')

  else:
    return redirect('/customer')

  '''
  query_adm = "SELECT * FROM users U, administrators A WHERE U.uid = A.user_uid"
  cursor_adm = g.conn.execute(query_adm)
  email_and_pwd_adm = []
  for user in cursor_adm:
    email_and_pwd_adm.append((user['email'],user['password']))
  cursor_adm.close()

  query_cst = "SELECT * FROM users U, customers C WHERE U.uid = C.user_uid"
  cursor_cst = g.conn.execute(query_cst)
  email_and_pwd_cst = []
  for user in cursor_cst:
    email_and_pwd_cst.append((user['email'],user['password']))
  cursor_cst.close()

  if   (email,password) in email_and_pwd_adm:
    # TODO : display "Login OK"
    return redirect('/administrator')
  elif (email,password) in email_and_pwd_cst:
    # TODO : display "Login OK"
    return redirect('/customer')
  else:
    # TODO : display "Invalid Login Info"
    return redirect('/')
  '''  


# Register process
@app.route('/add_user', methods=['POST'])
def add_user():
  
  name = request.form['name']
  email = request.form['email']
  
  # check if email already exists in DB
  q = "SELECT * FROM {name}".format(name='users')
  cursor = g.conn.execute(q)
  emails = []
  for result in cursor:
    emails.append(result['email'])
  cursor.close()

  if email in emails:
    # TODO : show your email is already in our DB
    print "Your email is already in our DB"
    return redirect('/register')

  # check if email is in valid form
  isNone = re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', email)
  if isNone == None:
    # TODO : show your email is not in valid format
    print "Your email is not in valid format"
    return redirect('/register')

  # md5 hashing for password
  password = hashlib.md5(request.form['password']).hexdigest()

  birthday = request.form['birthday']
  address = request.form['address']
  # TODO : check formats using regular expressions

  # TODO : avoid collision while adding
  q = "SELECT * FROM {name}".format(name='users')
  cursor = g.conn.execute(q)
  users = []
  for result in cursor:
    users.append(result)
  cursor.close()

  uid = cursor.rowcount + 1

  cmd = 'INSERT INTO users(uid, name, email, password) VALUES (:uid, :name, :email, :password)';
  g.conn.execute(text(cmd), uid = uid, name = name, email = email, password = password);
  # TODO : add to customers DB
  return redirect('/')



# Add another administrator process
@app.route('/add_admin', methods=['POST'])
def add_admin():
  
  name = request.form['name']

  # check if email already exists in DB
  q = "SELECT * FROM {name}".format(name='users')
  cursor = g.conn.execute(q)
  emails = []
  for result in cursor:
    emails.append(result['email'])
  cursor.close()

  if email in emails:
    # TODO : show your email is already in our DB
    print "Your email is already in our DB"
    return redirect('/register')

  # check if email is in valid form
  isNone = re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', email)
  if isNone == None:
    # TODO : show your email is not in valid format
    print "Your email is not in valid format"
    return redirect('/register')

  # md5 hashing for password
  password = hashlib.md5(request.form['password']).hexdigest()

  office = request.form['office']
  position = request.form['position']
  # TODO : check formats using regular expressions

  # TODO : avoid collision while adding
  q = "SELECT * FROM {name}".format(name='users')
  cursor = g.conn.execute(q)
  users = []
  for result in cursor:
    users.append(result)
  cursor.close()

  uid = cursor.rowcount + 1

  cmd = 'INSERT INTO users(uid, name, email, password) VALUES (:uid, :name, :email, :password)';
  g.conn.execute(text(cmd), uid = uid, name = name, email = email, password = password);
  # TODO : add to administrators DB
  return redirect('/administrator')



# Add coupon process
@app.route('/add_coupon', methods=['POST'])
def add_coupon():

  # TODO : find uid
  uid = 100
  code = request.form['code']
  discount = request.form['discount']
  exp = request.form['exp']
  time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
  # TODO : check formats using regular expressions

  # TODO : avoid collision while adding
  q = "SELECT * FROM {name}".format(name='coupons')
  cursor = g.conn.execute(q)
  coupons = []
  for result in cursor:
    coupons.append(result)
  cursor.close()

  cid = cursor.rowcount + 1

  cmd = 'INSERT INTO coupons(cid, uid, code, discount, expirartion_date, creation_time) VALUES (:cid, :uid, :code, :discount, :exp, :time)';
  g.conn.execute(text(cmd), cid = cid, uid = uid, code = code, discount = discount, exp = exp, time = time);
  return redirect('/administrator')



# Search process
@app.route('/search', methods=['POST'])
def search():
  name = request.form['name']

  query = 'SELECT * FROM restaurants WHERE name = ' + '\'' + name + '\''
  cursor_adm = g.conn.execute(query)
  restaurants = []
  for restaurant in cursor_adm:
    restaurants.append((restaurant['rid'], restaurant['name'], restaurant['category'], restaurant['address'], restaurant['url'], restaurant['tel']))
  cursor_adm.close()

  if not restaurants:
    session['no_result'] = True
    return redirect('/customer')

  session['no_result'] = False
  rid = restaurants[0][0]

  query = 'SELECT * FROM reviews WHERE rid = ' + str(rid)
  cursor_adm = g.conn.execute(query)
  reviews = []
  rating_sum = 0
  for review in cursor_adm:
    reviews.append((review['creation_time'], review['rating'], review['comment']))
    rating_sum += int(review['rating'])
  cursor_adm.close()

  avg_rating = 0
  if reviews:
    avg_rating = float(rating_sum) / len(reviews)

  session['book_rid'] = rid
  session['book_name'] = restaurants[0][1]

  return render_template('results.html', restaurant=restaurants[0], reviews=reviews, avg_rating=avg_rating)

# Book process
@app.route('/book', methods=['GET', 'POST'])
def book():
  global inventories
  query = 'SELECT * FROM inventories WHERE rid = ' + str(session['book_rid'])
  cursor_adm = g.conn.execute(query)
  inventory_id = 0
  inventories = []
  for inventory in cursor_adm:
    if inventory['quantity'] > 0:
      inventories.append((inventory_id, inventory['time'], inventory['table_size']))
      inventory_id += 1
  cursor_adm.close()

  return render_template('book.html', inventories=inventories, restaurant=session['book_name'])


@app.route('/confirm/<inventory_id>', methods=['GET', 'POST'])
def confirm(inventory_id):
  global inventories
  reservation = inventories[int(inventory_id)]
  rid = session['book_rid']
  time = reservation[1]
  table_size = reservation[2]

  uid = session['uid']

  query = 'UPDATE inventories SET quantity = quantity - 1 WHERE rid = ' + str(rid) + ' and time = ' + '\'' + str(time) + '\'' +' and table_size = ' + str(table_size) 
  g.conn.execute(query)

  cmd = 'INSERT INTO reservations(uid, rid, reserv_time, table_size) VALUES (:uid, :rid, :reserv_time, :table_size)';
  g.conn.execute(text(cmd), uid = uid, rid = rid, reserv_time = time, table_size = table_size);

  return render_template('confirmation.html')


if __name__ == "__main__":
  import click

  @click.command()
  @click.option('--debug', is_flag=True)
  @click.option('--threaded', is_flag=True)
  @click.argument('HOST', default='0.0.0.0')
  @click.argument('PORT', default=8111, type=int)
  def run(debug, threaded, host, port):
    """
    This function handles command line parameters.
    Run the server using

        python server.py

    Show the help text using

        python server.py --help

    """

    HOST, PORT = host, port
    print "running on %s:%d" % (HOST, PORT)
    app.run(host=HOST, port=PORT, debug=debug, threaded=threaded)


  run()
